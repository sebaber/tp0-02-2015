package ar.fiuba.tdd.tp0;

import java.util.LinkedList;

public interface IOperation {
	void operate(LinkedList<Float> operands);
}
