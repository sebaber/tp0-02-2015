package ar.fiuba.tdd.tp0;

import java.util.LinkedList;

public class MulAddOperation implements IOperation{

	@Override
	public void operate(LinkedList<Float> operands) {
		if(operands.isEmpty()) throw new IllegalArgumentException();
		float acum=operands.removeFirst();
		for(float operand : operands){
			acum += operand;
		}
		operands.clear();
		
		operands.push(acum);
	}

}
