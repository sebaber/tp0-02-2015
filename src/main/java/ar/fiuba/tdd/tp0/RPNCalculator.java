package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.LinkedList;

public class RPNCalculator {

	private HashMap<String,IOperation> mapOperations;
	private LinkedList<Float> operands;

	private void initOperations(){
		mapOperations = new HashMap<String, IOperation>();
		mapOperations.put("+",new AddOperation());
		mapOperations.put("-",new SubOperation());
		mapOperations.put("*",new MulOperation());
		mapOperations.put("/",new DivOperation());
		mapOperations.put("MOD",new ModOperation());
		mapOperations.put("++",new MulAddOperation());
		mapOperations.put("--",new MulSubOperation());
		mapOperations.put("**",new MulMulOperation());
		mapOperations.put("//",new MulDivOperation());
		operands = new LinkedList<Float>();		
	}

	public float eval(String expression) {
		String[] arrayExpresision;
		initOperations();
		try{
			arrayExpresision = expression.split(" ");
		}catch(NullPointerException e){
			throw new IllegalArgumentException();
		}
		for(int i=0;i<arrayExpresision.length;++i){
			IOperation operation = mapOperations.get(arrayExpresision[i]);
			if(operation != null){
				operation.operate(operands);
			}else operands.push(Float.parseFloat(arrayExpresision[i]));
		}
		return operands.remove();
	}

}
