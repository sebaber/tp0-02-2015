package ar.fiuba.tdd.tp0;

import java.util.LinkedList;

public class MulOperation implements IOperation{
	@Override
	public void operate(LinkedList<Float> operands) {
		float op2=operands.removeFirst();
		float op1=operands.removeFirst();
		operands.push(op1*op2);
	}
}