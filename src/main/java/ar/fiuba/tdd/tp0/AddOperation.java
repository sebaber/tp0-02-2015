package ar.fiuba.tdd.tp0;

import java.util.LinkedList;
import java.util.NoSuchElementException;

public class AddOperation implements IOperation{

	@Override
	public void operate(LinkedList<Float> operands) {
		try{
		float op2=operands.removeFirst();
		float op1=operands.removeFirst();
		operands.push(op1 +op2);
		}catch(NoSuchElementException e){
			throw new IllegalArgumentException();
		}
	}

}
